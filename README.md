# JGitWrapper

[![Release](https://img.shields.io/github/release/jitpack/gradle-simple.svg?label=gradle)](https://jitpack.io/#org.bitbucket.salab/jgitwrapper)
[![Kotlin](https://img.shields.io/badge/kotlin-1.0.0--beta--2422-blue.svg)](http://kotlinlang.org)
[![DUB](https://img.shields.io/dub/l/vibe-d.svg)](https://github.com/salab/jgitwrapper/blob/master/LICENSE)

A wrapper of JGit [http://www.eclipse.org/jgit/](http://www.eclipse.org/jgit/).

The goal is easy to call simple git commands,
so this will not support full features of JGit.

## Usage

# Add jitpack.io as Maven central repository.
```
repositories {
    // other repositories.  e.g. jcenter()
    maven { url "https://jitpack.io" }
}
```

# Add the dependency.

```
dependencies {
    compile "org.bitbucket.salab:jgitwrapper:${release_version}"
    // Each name of published tags represents ${release_version}.
    // You can choose one of them and use it.
}
```
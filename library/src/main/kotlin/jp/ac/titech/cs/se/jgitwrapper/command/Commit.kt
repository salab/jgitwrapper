package jp.ac.titech.cs.se.jgitwrapper.command

import org.eclipse.jgit.api.CommitCommand
import org.eclipse.jgit.lib.PersonIdent
import org.eclipse.jgit.revwalk.RevCommit

/**
 * Created by jmatsu on 2015/11/13.
 */
class Commit internal constructor(val commit: CommitCommand): GitCommand<RevCommit>(commit) {
    override fun precondition(): Boolean {
        return commit.run {
            message != null
        }
    }

    fun minimum(message: String) = apply { message(message) }

    fun message(msg: String) = apply { commit.setMessage(msg) }

    fun author(author: PersonIdent) = apply { commit.setAuthor(author) }

    fun committer(committer: PersonIdent) = apply { commit.setCommitter(committer) }

    fun amend(enable: Boolean = true) = apply { commit.setAmend(enable) }
}
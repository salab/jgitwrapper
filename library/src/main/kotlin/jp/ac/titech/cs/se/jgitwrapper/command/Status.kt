package jp.ac.titech.cs.se.jgitwrapper.command

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.StatusCommand
import org.eclipse.jgit.treewalk.FileTreeIterator
import org.eclipse.jgit.api.Status as OriginalStatus

/**
 * Created by jmatsu on 2015/11/13.
 */
class Status internal constructor(val status: StatusCommand, jgit: Git): GitCommand<OriginalStatus>(status) {
    init {
        status.setWorkingTreeIt(FileTreeIterator(jgit.repository))
    }

    override fun precondition() = true

    fun specify(vararg paths: String) = apply { paths.forEach { status.addPath(it) } }
}
package jp.ac.titech.cs.se.jgitwrapper.command

import jp.ac.titech.cs.se.jgitwrapper.util.GitUtil
import org.eclipse.jgit.api.DiffCommand
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.lib.Ref

/**
 * Created by jmatsu on 2015/11/13.
 */
class Diff internal constructor(val diff: DiffCommand, val jgit: Git): GitCommand<List<DiffEntry>>(diff) {
    override fun precondition() = true

    fun old(ref: String) = apply {
        GitUtil.getTreeIterator(jgit.repository, ref)?.apply {
            diff.setOldTree(this)
        }
    }

    fun new(ref: String) = apply {
        GitUtil.getTreeIterator(jgit.repository, ref)?.apply {
            diff.setNewTree(this)
        }
    }

    fun old(ref: Ref) = old(ref.name.trim())
    fun new(ref: Ref) = new(ref.name.trim())
}
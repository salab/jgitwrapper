package jp.ac.titech.cs.se.jgitwrapper.command

import org.eclipse.jgit.api.GitCommand

/**
 * Created by jmatsu on 2015/11/13.
 */
abstract class GitCommand<R> internal constructor(val cmd: GitCommand<R>) {
    fun execute(): R? = if (precondition()) cmd.call() else null

    abstract fun precondition(): Boolean
}
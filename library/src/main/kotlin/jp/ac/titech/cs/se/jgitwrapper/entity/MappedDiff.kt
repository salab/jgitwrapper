package jp.ac.titech.cs.se.jgitwrapper.entity

import org.eclipse.jgit.diff.DiffEntry

/**
 * Created by jmatsu on 2015/11/13.
 */
data class MappedDiff(val diffs: List<DiffEntry>) {
    val added: List<DiffEntry> by lazy {
        diffs.filter { it.changeType == DiffEntry.ChangeType.ADD }
    }
    val deleted: List<DiffEntry> by lazy {
        diffs.filter { it.changeType == DiffEntry.ChangeType.DELETE }
    }
    val modified: List<DiffEntry> by lazy {
        diffs.filter { it.changeType == DiffEntry.ChangeType.MODIFY }
    }
    val renamed: List<DiffEntry> by lazy {
        diffs.filter { it.changeType == DiffEntry.ChangeType.RENAME }
    }
    val copied: List<DiffEntry> by lazy {
        diffs.filter { it.changeType == DiffEntry.ChangeType.COPY }
    }
}
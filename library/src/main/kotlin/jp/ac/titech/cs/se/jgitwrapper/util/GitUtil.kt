package jp.ac.titech.cs.se.jgitwrapper.util

import jp.ac.titech.cs.se.jgitwrapper.entity.MappedDiff
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.treewalk.CanonicalTreeParser

/**
 * Created by jmatsu on 2015/11/13.
 */
object GitUtil {
    fun getTreeIterator(repo: Repository, ref: String) =
            repo.resolve(ref)?.run {
                CanonicalTreeParser().apply {
                    reset(repo.newObjectReader(), RevWalk(repo).parseTree(this@run))
                }
            }

    fun makeRef(count: Int) = HEAD + 0.until(count).map { "^" }.reduce { s1, s2 -> s1 + s2 }

    val HEAD: String
        get() = "HEAD"

    fun mapDiff(diffs: List<DiffEntry>) = MappedDiff(diffs)
}
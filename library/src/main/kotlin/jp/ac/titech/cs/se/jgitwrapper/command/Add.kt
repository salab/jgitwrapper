package jp.ac.titech.cs.se.jgitwrapper.command

import org.eclipse.jgit.api.AddCommand
import org.eclipse.jgit.dircache.DirCache

/**
 * Created by jmatsu on 2015/11/13.
 */
class Add internal constructor(val add: AddCommand): GitCommand<DirCache>(add) {
    override fun precondition(): Boolean = true

    fun all() = apply { add.addFilepattern("*") }

    fun pattern(pattern: String) = apply { add.addFilepattern(pattern) }
}
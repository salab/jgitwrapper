package jp.ac.titech.cs.se.jgitwrapper.config

import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session
import jp.ac.titech.cs.se.jgitwrapper.GitAPI
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.RepositoryBuilder
import org.eclipse.jgit.transport.JschConfigSessionFactory
import org.eclipse.jgit.transport.OpenSshConfig
import org.eclipse.jgit.transport.SshSessionFactory
import org.eclipse.jgit.util.FS
import java.io.File
import java.io.IOException

data class GitDir(val path: String?, val sshPath: String?) {
    private val sshConfig: SSHConfig = SSHConfig.create(this)

    val enabled: Boolean
        get() = path != null

    val jgit: Git? by lazy {
        try {
            val file = File(path)

            if (file.exists()) {
                RepositoryBuilder().findGitDir(file).build().run {
                    Git(this) // output
                }
            } else {
                null
            }
        } catch (e: IOException) {
            null
        }
    }

    fun toAPI(): GitAPI? =
            jgit?.run {
                SshSessionFactory.setInstance(ConfigSessionFactory(sshConfig))
                GitAPI(this@GitDir)
            }

    fun hasRemote() = jgit?.run {
        repository.config.getSubsections("remote").isNotEmpty()
    }.let { it ?: false }

    fun defaultRemote() = jgit?.run {
        repository.config.getSubsections("remote").firstOrNull()
    }

    fun currentBranch() = jgit?.repository?.branch

    data class SSHConfig private constructor(val sshDirPath: String?) {
        var identityFileName = "id_rsa"

        val available: Boolean
            get() = sshDirPath != null && sshDirPath.isNotEmpty()

        val privateKeyPath: String
            get() = "$sshDirPath${File.pathSeparator}$identityFileName"

        val knownHosts: String
            get() = "$sshDirPath${File.pathSeparator}known_hosts"

        companion object {
            fun create(gitDir: GitDir): SSHConfig {
                return SSHConfig(gitDir.sshPath)
            }
        }
    }

    class ConfigSessionFactory(val sshConfig: SSHConfig): JschConfigSessionFactory() {
        override fun configure(hc: OpenSshConfig.Host?, session: Session?) {
            session?.run {
                setConfig("StrictHostKeyChecking", "yes")
            }
        }

        override fun getJSch(hc: OpenSshConfig.Host?, fs: FS?): JSch? {
            return super.getJSch(hc, fs).apply {
                removeAllIdentity()
                if (sshConfig.available) {
                    hc?.apply {
                        addIdentity(sshConfig.privateKeyPath)
                        setKnownHosts(sshConfig.knownHosts)
                    }
                }
            }
        }
    }
}

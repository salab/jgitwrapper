package jp.ac.titech.cs.se.jgitwrapper.command

import org.eclipse.jgit.lib.Config
import org.eclipse.jgit.transport.RemoteConfig
import org.eclipse.jgit.transport.URIish

/**
 * Created by jmatsu on 2015/11/13.
 */
class Remote internal constructor(val config: Config) {
    fun add(name: String, url: String) =
            writeRemote(name, url) {
                it.isEmpty()
            }

    fun setUrl(name: String, url: String) =
            writeRemote(name, url) {
                true
            }


    private fun writeRemote(name: String, url: String, f: (String) -> Boolean): Boolean {
        return config.run {
            if (f.invoke(getString("remote", name, "url"))) {
                with(RemoteConfig(this, name)) {
                    addURI(URIish(url))
                    update(this@run)
                    return@run true
                }
            }

            return false
        }
    }
}
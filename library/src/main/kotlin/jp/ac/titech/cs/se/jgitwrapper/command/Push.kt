package jp.ac.titech.cs.se.jgitwrapper.command

import org.eclipse.jgit.api.PushCommand
import org.eclipse.jgit.transport.PushResult
import org.eclipse.jgit.transport.RefSpec

/**
 * Created by jmatsu on 2015/11/13.
 */
class Push internal constructor(val push: PushCommand, private var source: String?): GitCommand<Iterable<PushResult>>(push) {
    companion object {
        val src_format = "refs/heads/%s"
        val dest_format = "refs/heads/%s/%s"
    }

    private var dest: String? = null
    private var remote: String = "origin"

    fun minimum(dest: String, remote: String) =
            apply {
                dest(dest)
                remote(remote)
            }

    fun force(force: Boolean) = apply { push.setForce(force) }

    fun remote(name: String) = apply { remote = name; push.setRemote(remote) }

    fun source(name: String) = apply { source = name }

    fun dest(name: String) = apply { dest = name }

    override fun precondition(): Boolean {
        return dest?.apply {
            source?.run {
                push.setRefSpecs(refspec(this, this@apply))
            }
        }.let { !it.isNullOrEmpty() }
    }

    private fun refspec(srcBranch: String, destBranch: String) =
            RefSpec().apply {
                setSource(src_format.format(srcBranch))
                setDestination(dest_format.format(remote, destBranch))
            }
}
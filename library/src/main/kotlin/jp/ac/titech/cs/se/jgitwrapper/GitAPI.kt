package jp.ac.titech.cs.se.jgitwrapper

import jp.ac.titech.cs.se.jgitwrapper.command.*
import jp.ac.titech.cs.se.jgitwrapper.config.GitDir
import org.eclipse.jgit.api.Git
import java.io.File

/**
 * Created by jmatsu on 2015/11/13.
 */
data class GitAPI(val gitDir: GitDir) {
    val api = gitDir.jgit!!

    companion object {
        fun init(dir: File) =
                Git.init().setDirectory(dir).call().run {
                    GitDir(dir.absolutePath, null)
                }

        fun clone(url: String, dir: File) =
                Git.cloneRepository().apply {
                    setURI(url)
                    setDirectory(dir)
                }.call().run {
                    GitDir(dir.absolutePath, null)
                }
    }

    fun remote() = Remote(api.repository.config)

    fun add() = Add(api.add())

    fun commit() = Commit(api.commit())

    fun push() = Push(api.push(), gitDir.currentBranch())

    fun status() = Status(api.status(), api)
}
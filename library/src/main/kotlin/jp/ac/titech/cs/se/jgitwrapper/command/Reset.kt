package jp.ac.titech.cs.se.jgitwrapper.command

import jp.ac.titech.cs.se.jgitwrapper.util.GitUtil
import org.eclipse.jgit.api.ResetCommand
import org.eclipse.jgit.lib.Ref

/**
 * Created by jmatsu on 2015/11/13.
 */
class Reset internal constructor(val reset: ResetCommand): GitCommand<Ref>(reset) {
    override fun precondition(): Boolean {
        throw UnsupportedOperationException()
    }

    /**
     * Specify a file path to reset
     * @param path : the path of the affected file
     */
    fun add(path: String) = apply { reset.addPath(path) }

    /**
     * Move index to specified ref.
     * @param ref : e.g. HEAD^, commit id
     */
    fun ref(ref: String) = apply { reset.setRef(ref) }

    fun times(count: Int) = apply { reset.setRef(GitUtil.makeRef(count)) }

    fun mixed() = apply { reset.setMode(ResetCommand.ResetType.MIXED) }
    fun soft() = apply { reset.setMode(ResetCommand.ResetType.SOFT) }
    fun hard() = apply { reset.setMode(ResetCommand.ResetType.HARD) }
    fun merge() = apply { reset.setMode(ResetCommand.ResetType.MERGE) }
    fun keep() = apply { reset.setMode(ResetCommand.ResetType.KEEP) }
}